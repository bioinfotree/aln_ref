#!/bin/bash

# Copyright Gabriele Magris 2014 <asso89@hotmail.com>

# pipeline used to realign the bam files:
# after alignement the bam file has been sorted + rmdup
# now realignment will be run

# show usage subroutine
show_usage() {
echo "Usage: ${0##/*} [sortfile] [fastafile] [statistics] [threads] [outdir]"
echo
echo "sortfile: bam file to be used for the realignment without the extension  (previously sorted and rmdup)"
echo "fastafile: reference fasta file (used for the realignment)"
echo "statistics: folder where to redirect the sterr and stout (create a folder with the file name)"
echo "threads: number of threads to be used for the realignment"
echo "outdir: directory where the output files should be addresses"
exit
}

# minimum number of arguments needed by this program
MINARGS=5

# get the number of command-line arguments given
ARGC=$#

# check to make sure enough arguments were given or exit
if [[ $ARGC -lt $MINARGS ]] ; then
 echo
 show_usage
fi


sortfile=$1
fastafile=$2
statistic=$3
threads=$4
outdir=$5

echo $sortfile
echo $outdir
vaR=`basename $sortfile`
var=${vaR/_mer*/}
stat_expdir=$statistic/$var
mkdir -p $stat_expdir
tmp_dir="$(mktemp -d XXXX)"

# control step to check if the .dict file is present or not and create it if necessary
# in the folder where the fasta reference sequence
inp_di=${fastafile/.fasta/}
inp_dict=$inp_di.dict

# chek if the file has something written inside
if test ! -s $inp_dict; then bb=$(echo 'si'); fi

# if the files is empty or missing, then run the command line to generate it
if [ ! -z "$bb" ] ; then 
	java -jar ${PICARDTOOLS_ROOT}/CreateSequenceDictionary.jar R=$fastafile O=$inp_di".dict";
fi

cd $outdir
RG_name=$var"_merged"

########## CLEAN SAM ################

# clean sam to perform fixs-up
java -Xmx10g -Djava.io.tmpdir=$tmp_dir -jar ${PICARDTOOLS_ROOT}/CleanSam.jar \
		INPUT=${sortfile}.bam \
		OUTPUT=${sortfile}.tmp.bam \
		VALIDATION_STRINGENCY=LENIENT \
		2> $stat_expdir/$var"_real".txt


# fix mate information
java -Xmx10g -XX:ParallelGCThreads=$threads -jar ${PICARDTOOLS_ROOT}/FixMateInformation.jar \
		INPUT=${sortfile}.tmp.bam \
		OUTPUT=${sortfile}.tmp.2.bam \
		TMP_DIR=$tmp_dir \
		SORT_ORDER=coordinate \
		CREATE_INDEX=True \
		VALIDATION_STRINGENCY=LENIENT \
		2>> $stat_expdir/$var"_real".txt

############# INTERVALS calculation ################
java -Xmx10g -Djava.io.tmpdir=$tmp_dir -jar $GATK_ROOT/GenomeAnalysisTK.jar \
		-I ${sortfile}.tmp.2.bam \
		-R $fastafile \
		-T RealignerTargetCreator \
		-o ${sortfile}_uniq.intervals \
		--num_threads $threads \
		>> $stat_expdir/$var"_real".txt 2>&1



########## REALIGNMENT ################
java -Xmx10g -Djava.io.tmpdir=$tmp_dir -jar $GATK_ROOT/GenomeAnalysisTK.jar \
		-I ${sortfile}.tmp.2.bam \
		-R $fastafile \
		-T IndelRealigner \
		-targetIntervals ${sortfile}_uniq.intervals \
		-o ${sortfile}_realigned.bam \
		>> $stat_expdir/$var"_real".txt 2>&1

rm ${sortfile}.tmp.*


# creo un file sam in cui ci sono solo le reads allineate in modo unico 
usamfile=${sortfile/_merge_sort/_realigned_U}".sam"
ubamfile=${usamfile/_U.sam/_U.bam}
# sameusam=${ubamfile/_U.bam/_U_samechr.sam}
# sameubam=${sameusam/_U_samechr.sam/_U_samechr.bam}
usortfile=${ubamfile/_U.bam/_sort}
statfile="mapping_"${usortfile/_sort/.txt}
insertstatfile=${samfile/.sam/_insertsize.txt}

realnfile=${sortfile}_realigned

# index sorted alignment for fast random access/create a file .bai
samtools index ${realnfile}.bam

# create header
samtools view -H ${realnfile}.bam > ${realnfile}_U.sam

samtools view ${realnfile}.bam | grep "XT:A:U" >> ${realnfile}_U.sam

samtools view -bSq 10 ${realnfile}_U.sam > ${realnfile}_U.bam
# samtools import $fastafile $usamfile $ubamfile
# samtools view ${ubamfile} | cut -f2 |sort|uniq -c > $statfile

samtools index ${realnfile}_U.bam

# selection of uniquely aligned in read pairs
# SAM_PU=${usamfile/_U.sam/_PU.sam}
# BAM_PU=${ubamfile/_U.bam/_PU.bam}
# samtools view -H $ubamfile > $SAM_PU
# samtools view $ubamfile | awk '{ if ( $2 == "99" || $2 == "147" || $2 == "83" || $2 == "163" ) {print $0} }' >> $SAM_PU
# samtools view -bS $SAM_PU > $BAM_PU
# rm $SAM_PU

# create plots plot
HIST=$var"_insert".pdf
HIST2=$var"_insert".txt
java -Xmx2g -XX:MaxPermSize=512m -jar ${PICARDTOOLS_ROOT}/CollectInsertSizeMetrics.jar INPUT=${realnfile}_U.bam HISTOGRAM_FILE=$HIST OUTPUT=$HIST2 TMP_DIR=$tmp_dir VALIDATION_STRINGENCY=LENIENT 2>>$stat_expdir/$var"_samt_stat".txt

# corrected the output redirection
# use qaTools to estimate insert size
computeInsertSizeHistogram -s ${realnfile}_U.bam ${realnfile}"_U_insert.txt" >$stat_expdir/$var"_ins_cov".txt

qaCompute ${realnfile}_U.bam ${realnfile}"_U_coverage.txt" &>>$stat_expdir/$var"_ins_cov".txt

rm ${realnfile}_U.sam

# corrected the path of the coverage.sh
for OLDBAMFILE in ${sortfile}_realigned.bam ${realnfile}_U.bam; do
	mydir=$(pwd)
	covdir=${mydir/alignments/coverage}
	mkdir -p $covdir
	BAMFILE=${mydir}/${OLDBAMFILE}
	coverage ${fastafile} ${BAMFILE} >>$stat_expdir/$var"_cov_wig".txt 2>&1
	COVFILE=${BAMFILE/alignments/coverage}
	WIGFILE=${COVFILE/.bam/.wig}
	zcat $WIGFILE | coverage_WIG --WIG - >> $stat_expdir/$var"_cov_wig".txt 2>&1
done

rm -r $tmp_dir