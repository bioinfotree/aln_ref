#!/bin/bash

# Copyright Gabriele Magris 2014 <asso89@hotmail.com>

# show usage subroutine
show_usage() {
echo "Usage: ${0##/*} [bam] [outdir] [reference] [log_dir]"
echo
echo "bam: target alignment"
echo "outdir: output directory"
echo "reference: reference sequence to be used for the alignment"
echo "log_dir: directory containing logs"
exit
}

# minimum number of arguments needed by this program
MINARGS=4

# get the number of command-line arguments given
ARGC=$#

# check to make sure enough arguments were given or exit
if [[ $ARGC -lt $MINARGS ]] ; then
 echo
 show_usage
fi

bam=$1
outdir=$2
reference=$3
statistic=$4
threads=$THREADNUM
max_insert_size=1000
histogram_width=null


mkdir -p $outdir
mkdir -p $statistic/${SAMPLE_NAME}
stat_expdir=$statistic/${SAMPLE_NAME}


# build bam index
mkdir -p ${SAMPLE_NAME}_tmp;
java -Xmx10g \
-Djava.io.tmpdir=$outdir/${SAMPLE_NAME}_tmp \
-jar $PICARDTOOLS_ROOT/picard.jar BuildBamIndex \
INPUT=${bam} \
TMP_DIR=${outdir}/${SAMPLE_NAME}_tmp \
VALIDATION_STRINGENCY=LENIENT \
3>&1 1>&2 2>&3 \
| tee -a $stat_expdir/${SAMPLE_NAME}"_picard_stat.txt" \
&& mv $outdir/"${bam%.bam}.bai" $outdir/${bam}.bai

# # [PIPELINE]
# # name: Alignment refinement metrics picard
# # codename: alignment-refinement-metrics-picard
# # code: ce92a9bb-c78b-4204-8b00-2d4795eaf61a
# # [step.s10]
# # label: HS Metrics
# java -Xmx10g \
# -Djava.io.tmpdir=$outdir/${SAMPLE_NAME}_tmp \
# -jar $PICARDTOOLS_ROOT/picard.jar CalculateHsMetrics \
# INPUT=$outdir/${SAMPLE_NAME}.clean.fixmate.nodup.reorder.bam \
# OUTPUT=$outdir/${SAMPLE_NAME}.clean.fixmate.nodup.reorder.hs_metrics.txt \
# REFERENCE_SEQUENCE=${reference} \
# PER_TARGET_COVERAGE=$outdir/${SAMPLE_NAME}.clean.fixmate.nodup.reorder.target_hs_coverage.txt \
# # BAIT_INTERVALS=$outdir/${SAMPLE_NAME}.clean.fixmate.nodup.reorder.intervals_header.bed \
# # TARGET_INTERVALS=$outdir/${SAMPLE_NAME}.clean.fixmate.nodup.reorder.intervals_enlarged_header.bed \
# TMP_DIR=${outdir}/${SAMPLE_NAME}_tmp \
# VALIDATION_STRINGENCY=LENIENT \
# 3>&1 1>&2 2>&3 \
# | tee -a $stat_expdir/${SAMPLE_NAME}"_picard_stat.txt"



# Insert Size Metrics
# recomended use of CollectInsertSizeMetrics from picard v 1.88
mkdir -p ${SAMPLE_NAME}_tmp;
java -Xmx2g -XX:MaxPermSize=512m -jar /iga/stratocluster/packages/sw/bio/picard-tools/1.88/CollectInsertSizeMetrics.jar \
REFERENCE_SEQUENCE=${reference} \
HISTOGRAM_FILE="${bam%.bam}.insert_size.pdf" \
INPUT=${bam} \
OUTPUT="${bam%.bam}.insert_size.txt" \
VALIDATION_STRINGENCY=SILENT \
HISTOGRAM_WIDTH=${histogram_width} \
ASSUME_SORTED=false \
TMP_DIR=${outdir}/${SAMPLE_NAME}_tmp \
3>&1 1>&2 2>&3 \
| tee -a $stat_expdir/${SAMPLE_NAME}"_picard_stat.txt"


# [PIPELINE]
# name: Alignment refinement metrics picard
# codename: alignment-refinement-metrics-picard
# code: ce92a9bb-c78b-4204-8b00-2d4795eaf61a
# [step.s9]
# label: Alignment Summary Metrics
mkdir -p ${SAMPLE_NAME}_tmp;
java -Xmx10g \
-Djava.io.tmpdir=$outdir/${SAMPLE_NAME}_tmp \
-jar $PICARDTOOLS_ROOT/picard.jar CollectAlignmentSummaryMetrics \
INPUT=${bam} \
OUTPUT="${bam%.bam}.alignment_summary.txt" \
MAX_INSERT_SIZE=${max_insert_size} \
ADAPTER_SEQUENCE=null \
METRIC_ACCUMULATION_LEVEL=ALL_READS \
REFERENCE_SEQUENCE=${reference} \
VALIDATION_STRINGENCY=LENIENT \
TMP_DIR=${outdir}/${SAMPLE_NAME}_tmp \
3>&1 1>&2 2>&3 \
| tee -a $stat_expdir/${SAMPLE_NAME}"_picard_stat.txt"


# compute coverage in wig format
nt-compute-profile --input-file $outdir/${bam} \
--fasta $reference \
--output-file $outdir/"${bam%.bam}.wig"

# print histogram of coverage. Use ylim="0:genome_covered"
# genome_covered can be found in coverage.hist
cov_plot.R \
--title="coverage" \
--ylim="0:500" \
--xlim="0:50" \
--max-cov=20 \
--output-file=$outdir/"${bam%.bam}.cov_wig.pdf" \
<$outdir/"${bam%.bam}.wig"

# compress wig
gzip -c <$outdir/"${bam%.bam}.wig" >$outdir/"${bam%.bam}.wig.gz" \
&& rm $outdir/"${bam%.bam}.wig"

# calculate coverage from wig and histogram
paste -s -d "\n" \
<(zcat $outdir/"${bam%.bam}.wig.gz" | coverage_WIG --WIG -) \
<(zcat $outdir/"${bam%.bam}.wig.gz" | histogram.pl --wig -) \
>$outdir/"${bam%.bam}.cov_wig.txt" 2>&1


# compute per chr coverage
qaCompute $outdir/${bam} \
$outdir/"${bam%.bam}.per_chr_coverage.txt" \
2>&1 \
| tee -a $stat_expdir/${SAMPLE_NAME}"_qaCompute_stat.txt"


# # name: BWAaln_PE-SE_metrics_RG
# # codename: bwaaln-pe-se-metrics-rg
# # code: 71b3f899-3498-4b98-9595-d2a5c9f43cb5
# # [step.s11]
# # label: bam_fingerprint
# bamFingerprint \
# --bamfiles ${bam} \
# --plotFile "${bam%.bam}.fingerprint.png" \
# --numberOfSamples 200000 \
# --numberOfProcessors $threads