#!/bin/bash

# Copyright Gabriele Magris 2014 <asso89@hotmail.com>

# script for generating a WIG file starting from a BAM file and for calculating the coverege


show_usage() {
echo "Usage: ${0##/*} [ref_file] [alignment_file]"
echo
echo "ref_file: reference file in fasta format"
echo "alignment_file: full name of alignment file"
exit
}

# minimum number of arguments needed by this program
MINARGS=2

# get the number of command-line arguments given
ARGC=$#

# check to make sure enough arguments were given or exit
if [[ $ARGC -lt $MINARGS ]] ; then
 echo
 show_usage
fi

REF=$1
BAMFILE=$2
COVFILE=${BAMFILE/alignments/coverage}
COVFILE=${COVFILE/.bam/.cov}
WIGFILE=${COVFILE/.cov/.wig}

COVDIR=`dirname $COVFILE`

mkdir -p $COVDIR

compute_profile --input-file $BAMFILE --sam-format --fasta $REF --output-file $WIGFILE

coverage_WIG --WIG $WIGFILE >> $COVFILE

gzip $WIGFILE