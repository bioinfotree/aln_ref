#!/bin/env perl

# Copyright 2013 Cristian Delfabbro <delfabbro@appliedgenomics.org>

##########################################################
# Description: takes a wig and computes average coverage #
##########################################################

use warnings;
use strict;
use Getopt::Long;
use Pod::Usage;
use Switch;
my $WIG;
my $help    = 0;

GetOptions(
        'WIG=s' => \$WIG,
        'help|?'       => \$help
) or pod2usage(2);
pod2usage(1) if $help;
pod2usage(1) if (!defined($WIG));

my $cov = 0;
my $zero_cov = 0;
my $one_cov = 0;
open(WIG, $WIG) or die("Could not open $WIG\n");
while (my $line = <WIG>) {
	chomp $line;
	if ($line =~ /fixed/) { next; }
	$cov += $line;
	$zero_cov++;
	if ($line > 0) { $one_cov++; }
}
close WIG;

print "# Genome size\tGenome covered\tSum of coverage\n";
print $zero_cov."\t".$one_cov."\t".$cov."\n";

__END__


=head1 GFF Statistics

coverage.pl - Using this script

=head1 SYNOPSIS

perl coverage.pl [--WIG <filename>] [--help]

  Options:
    --WIG <filename>		coverage file in WIG format
    --help			print this help message

output on standard output (redirect, if necessary)
