#!/bin/bash

# TODO:

# Copyright Gabriele Magris 2014 <asso89@hotmail.com>
# script for align reads on a reference sequence using BWA and compute profile of coverage

# show usage subroutine
show_usage() {
echo "Usage: ${0##/*} [read1] [read2] [outdir] [RG] [reference] [log_dir]"
echo
echo "read1: full path to read 1"
echo "read2: full path to read 2"
echo "outdir: output directory"
echo "RG: full string read group for picard"
echo "reference: reference sequence to be used for the alignment"
echo "log_dir: directory containing logs"
echo "set SAMPLE_NAME environment variable in order to refer to the correct fastq file"
exit
}

# minimum number of arguments needed by this program
MINARGS=5

# get the number of command-line arguments given
ARGC=$#

# check to make sure enough arguments were given or exit
if [[ $ARGC -lt $MINARGS ]] ; then
 echo
 show_usage
fi

read1=$1
read2=$2
outdir=$3
RG=$4
reference=$5
statistic=$6


mkdir -p $outdir
mkdir -p $statistic/${SAMPLE_NAME}
stat_expdir=$statistic/${SAMPLE_NAME}

# created the necessary index in order to use the fasta sequence as genome for making alignments 
# need to run the bwa index only if it doesn't exist, eitherwise it will give error
inp_pac=$reference.pac
inp_sa=$reference.sa
if test ! -s $inp_pac ; then bb=$(echo 'si'); fi
if test ! -s $inp_sa ; then b=$(echo 'si'); fi

# if the files are present, 
if [ ! -z "$bb" ] && [ ! -z "$b" ] ; then bwa index $reference ; fi

# check if the .fai index file exist
inp_fai=$reference.fai
if test ! -s $inp_fai ; then bb=$(echo 'si'); fi

if [ ! -z "$bb" ] ; then samtools faidx $reference ; fi

# [PIPELINE]
# name: BWAaln_PE-SE_metrics_RG
# codename: bwaaln-pe-se-metrics-rg
# code: 71b3f899-3498-4b98-9595-d2a5c9f43cb5
# [step.s1] bwa alignment paired-ends
bwa aln -t $THREADNUM \
${reference} ${read1} >$outdir/${SAMPLE_NAME}_1.sai \
2>>$stat_expdir/${SAMPLE_NAME}"_bwa_stat.txt"
bwa aln -t $THREADNUM \
${reference} ${read2} >$outdir/${SAMPLE_NAME}_2.sai \
2>>$stat_expdir/${SAMPLE_NAME}"_bwa_stat.txt"

# [PIPELINE]
# name: BWAaln_PE-SE_metrics_RG
# codename: bwaaln-pe-se-metrics-rg
# code: 71b3f899-3498-4b98-9595-d2a5c9f43cb5
# [step.s3] bwa alignment single-ends
# bwa aln -t $THREADNUM $reference $dir/${SAMPLE_NAME}_unpaired.fastq.gz >$outdir/${SAMPLE_NAME}_unpaired.sai 2>>$stat_expdir/${SAMPLE_NAME}"_bwa_stat.txt"

# [PIPELINE]
# name: BWAaln_PE-SE_metrics_RG
# codename: bwaaln-pe-se-metrics-rg
# code: 71b3f899-3498-4b98-9595-d2a5c9f43cb5
# [step.s2] sampe
bwa sampe $reference $outdir/${SAMPLE_NAME}_1.sai $outdir/${SAMPLE_NAME}_2.sai ${read1} ${read2} \
| samtools view -bS - >$outdir/${SAMPLE_NAME}.bam \
2>>$stat_expdir/${SAMPLE_NAME}"_sampe_stat.txt"
rm $outdir/${SAMPLE_NAME}_1.sai $outdir/${SAMPLE_NAME}_2.sai

# [PIPELINE]
# name: BWAaln_PE-SE_metrics_RG
# codename: bwaaln-pe-se-metrics-rg
# code: 71b3f899-3498-4b98-9595-d2a5c9f43cb5
# [step.s4] samse
# bwa samse $reference $outdir/${SAMPLE_NAME}_unpaired.sai $dir/${SAMPLE_NAME}_unpaired.fastq.gz | samtools view -bS - >$outdir/${SAMPLE_NAME}_unpaired.bam 2>>$stat_expdir/${SAMPLE_NAME}"_samse_stat.txt"
# rm $outdir/${SAMPLE_NAME}_unpaired.sai

# [PIPELINE]
# name: BWAaln_PE-SE_metrics_RG
# codename: bwaaln-pe-se-metrics-rg
# code: 71b3f899-3498-4b98-9595-d2a5c9f43cb5
# [step.s5] sort bam
samtools sort -o -@ ${THREADNUM} $outdir/${SAMPLE_NAME}.bam - >$outdir/${SAMPLE_NAME}.sort.bam
2>>$stat_expdir/${SAMPLE_NAME}"_samtools_stat.txt"
rm $outdir/${SAMPLE_NAME}.bam

# remove duplicates
samtools rmdup -S $outdir/${SAMPLE_NAME}.sort.bam $outdir/${SAMPLE_NAME}.sort.nodup.bam \
2>>$stat_expdir/${SAMPLE_NAME}"_samtools_stat.txt"
rm $outdir/${SAMPLE_NAME}.sort.bam



# select unique alignments
samtools view -H $outdir/${SAMPLE_NAME}.sort.nodup.bam >$outdir/${SAMPLE_NAME}.sort.nodup.uniq.sam
samtools view $outdir/${SAMPLE_NAME}.sort.nodup.bam \
| grep "XT:A:U" >>$outdir/${SAMPLE_NAME}.sort.nodup.uniq.sam

samtools view -bSq 10 $outdir/${SAMPLE_NAME}.sort.nodup.uniq.sam \
| samtools sort -o -@ ${THREADNUM} - - >$outdir/${SAMPLE_NAME}.sort.nodup.uniq.bam \
2>>$stat_expdir/${SAMPLE_NAME}"_samtools_stat.txt"

rm $outdir/${SAMPLE_NAME}.sort.nodup.uniq.sam



# [PIPELINE]
# name: BWAaln_PE-SE_metrics_RG
# codename: bwaaln-pe-se-metrics-rg
# code: 71b3f899-3498-4b98-9595-d2a5c9f43cb5
# [step.s6] clean_sam
mkdir -p ${SAMPLE_NAME}_tmp;
java -Xmx2g \
-Djava.io.tmpdir=$outdir/${SAMPLE_NAME}_tmp \
-jar $PICARDTOOLS_ROOT/picard.jar CleanSam \
INPUT=$outdir/${SAMPLE_NAME}.sort.nodup.uniq.bam \
OUTPUT=$outdir/${SAMPLE_NAME}.sort.nodup.uniq.clean.bam \
VALIDATION_STRINGENCY=STRICT \
TMP_DIR=${outdir}/${SAMPLE_NAME}_tmp \
3>&1 1>&2 2>&3 \
| tee -a $stat_expdir/${SAMPLE_NAME}"_picard_stat.txt"

rm $outdir/${SAMPLE_NAME}.sort.nodup.uniq.bam


# [PIPELINE]
# name: BWAaln_PE-SE_metrics_RG
# codename: bwaaln-pe-se-metrics-rg
# code: 71b3f899-3498-4b98-9595-d2a5c9f43cb5
# [step.s7] Add_RG_tags
mkdir -p ${SAMPLE_NAME}_tmp;
java -Xmx4g \
-Djava.io.tmpdir=$outdir/${SAMPLE_NAME}_tmp \
-jar $PICARDTOOLS_ROOT/picard.jar AddOrReplaceReadGroups \
INPUT=$outdir/${SAMPLE_NAME}.sort.nodup.uniq.clean.bam \
OUTPUT=$outdir/${SAMPLE_NAME}.sort.nodup.uniq.clean.RG.bam \
${RG} \
VALIDATION_STRINGENCY=LENIENT \
TMP_DIR=$outdir/${SAMPLE_NAME}_tmp \
3>&1 1>&2 2>&3 \
| tee -a $stat_expdir/${SAMPLE_NAME}"_picard_stat.txt"

rm $outdir/${SAMPLE_NAME}.sort.nodup.uniq.clean.bam




# [PIPELINE]
# name: Alignment refinement metrics picard
# codename: alignment-refinement-metrics-picard
# code: ce92a9bb-c78b-4204-8b00-2d4795eaf61a
# [step.s5]
# label: Fix Mate Information
# fix mate information
mkdir -p ${SAMPLE_NAME}_tmp;
java -Xmx10g \
-Djava.io.tmpdir=$outdir/${SAMPLE_NAME}_tmp \
-jar $PICARDTOOLS_ROOT/picard.jar FixMateInformation \
INPUT=$outdir/${SAMPLE_NAME}.sort.nodup.uniq.clean.RG.bam \
OUTPUT=$outdir/${SAMPLE_NAME}.sort.nodup.uniq.clean.RG.fixmate.bam \
TMP_DIR=${outdir}/${SAMPLE_NAME}_tmp \
SORT_ORDER=coordinate \
VALIDATION_STRINGENCY=LENIENT \
3>&1 1>&2 2>&3 \
| tee -a $stat_expdir/${SAMPLE_NAME}"_picard_stat.txt"

rm $outdir/${SAMPLE_NAME}.sort.nodup.uniq.clean.RG.bam


# # [PIPELINE]
# # name: Alignment refinement metrics picard
# # codename: alignment-refinement-metrics-picard
# # code: ce92a9bb-c78b-4204-8b00-2d4795eaf61a
# # [step.s6]
# # label: Mark Duplicates
# mkdir -p ${SAMPLE_NAME}_tmp;
# java -Xmx10g \
# -Djava.io.tmpdir=$outdir/${SAMPLE_NAME}_tmp \
# -jar $PICARDTOOLS_ROOT/picard.jar MarkDuplicates \
# INPUT=$outdir/${SAMPLE_NAME}.clean.fixmate.bam \
# OUTPUT=$outdir/${SAMPLE_NAME}.clean.fixmate.nodup.bam \
# REMOVE_DUPLICATES=True \
# METRICS_FILE=$outdir/${SAMPLE_NAME}.clean.fixmate.nodup.metrics.txt \
# TMP_DIR=${outdir}/${SAMPLE_NAME}_tmp \
# VALIDATION_STRINGENCY=LENIENT \
# 3>&1 1>&2 2>&3 \
# | tee -a $stat_expdir/${SAMPLE_NAME}"_picard_stat.txt"
# 
# rm $outdir/${SAMPLE_NAME}.clean.fixmate.bam


# if the files is empty or missing, then run the command line to generate it
if [ ! -s "${reference}.dict" ] ; then
	mkdir -p ${SAMPLE_NAME}_tmp;
	java -Xmx2g \
	-Djava.io.tmpdir=$outdir/${SAMPLE_NAME}_tmp \
	-jar $PICARDTOOLS_ROOT/picard.jar CreateSequenceDictionary \
	REFERENCE=${reference} \
	OUTPUT="${reference}.dict"; \
	VALIDATION_STRINGENCY=STRICT \
	TMP_DIR=${outdir}/${SAMPLE_NAME}_tmp \
	3>&1 1>&2 2>&3 \
	| tee -a $stat_expdir/${SAMPLE_NAME}"_picard_stat.txt"
fi


# [PIPELINE]
# name: Alignment refinement metrics picard
# codename: alignment-refinement-metrics-picard
# code: ce92a9bb-c78b-4204-8b00-2d4795eaf61a
# [step.s7]
# label: Reorder SAM
mkdir -p ${SAMPLE_NAME}_tmp;
java -Xmx10g \
-Djava.io.tmpdir=$outdir/${SAMPLE_NAME}_tmp \
-jar $PICARDTOOLS_ROOT/picard.jar ReorderSam \
INPUT=$outdir/${SAMPLE_NAME}.sort.nodup.uniq.clean.RG.fixmate.bam \
OUTPUT=$outdir/${SAMPLE_NAME}.sort.nodup.uniq.clean.RG.fixmate.reord.bam \
REFERENCE=${reference} \
TMP_DIR=${outdir}/${SAMPLE_NAME}_tmp \
VALIDATION_STRINGENCY=LENIENT \
3>&1 1>&2 2>&3 \
| tee -a $stat_expdir/${SAMPLE_NAME}"_picard_stat.txt"

rm $outdir/${SAMPLE_NAME}.sort.nodup.uniq.clean.RG.fixmate.bam