# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

SAMPLE_NAME ?= 
READS_DIR ?= 
RG ?= 
REFERENCE ?= 
INSERT_SIZE_HISTOGRAM_WIDTH ?= null
MAX_INSERT_SIZE ?= 100000
MINIMUM_PCT ?= 0.05
MAX_MB ?= 25
MAX_COV ?= 50


context prj/aln_genomes


log:
	mkdir -p $@

tmp:
	mkdir -p $@

reference.fasta:
	ln -sf $(REFERENCE) $@


BAM = $(addsuffix .bam,$(SAMPLES))
.PHONY:  install_links
%.bam:
	@echo installing links ... \
	$(foreach SAMPLE,$(SAMPLES), \
		$(shell ln -sf ../phase_2/$(SAMPLE).RG.bam.sort $(SAMPLE).bam) \
	)

LOCAL_SAMPLES = $(SAMPLES)

import calculate_stat


.META: fstats.all
	1	sample
	2	in total (QC-passed reads + QC-failed reads)
	3	duplicates
	4	mapped
	5	paired in sequencing
	6	read1
	7	read2
	8	properly paired
	9	with itself and mate mapped
	10	singletons
	11	with mate mapped to a different chr
	12	with mate mapped to a different chr (mapQ>=5)

fstats.all: $(FSTATS)
	cat $^ >$@

.META: cov_wig.all
	1	sample
	2	genome size
	3	genome covered
	4	sum of coverage

cov_wig.all: $(COV_WIG_TXT)
	cat $^ >$@


ALL +=  log \
	$(BAM) \
	$(FSTATS) \
	$(COV_WIG_TXT) \
	fstats.all \
	cov_wig.all

INTERMEDIATE += reference.fasta

CLEAN += 