# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>


# build bam index
BAI = $(addsuffix .bam.bai,$(LOCAL_SAMPLES))
%.bam.bai: log tmp %.bam
	$(load_modules); \
	java -Xmx4g \
	-Djava.io.tmpdir=$^2 \
	-jar $$PICARDTOOLS_ROOT/picard.jar BuildBamIndex \
	INPUT=$^3 \
	OUTPUT=$@ \
	TMP_DIR=$^2 \
	VALIDATION_STRINGENCY=LENIENT \
	3>&1 1>&2 2>&3 \
	| tee $</picard-BuildBamIndex.$@.log


.META: %.fstats
	1	sample
	2	in total (QC-passed reads + QC-failed reads)
	3	duplicates
	4	mapped (98.79%:-nan%)
	5	paired in sequencing
	6	read1
	7	read2
	8	properly paired (97.16%:-nan%)
	9	with itself and mate mapped
	10	singletons (0.83%:-nan%)
	11	with mate mapped to a different chr
	12	with mate mapped to a different chr (mapQ>=5)

# calculates statistics about flags 
FSTATS = $(addsuffix .fstats,$(LOCAL_SAMPLES))
%.fstats: %.bam
	$(load_modules); \
	samtools flagstat $< \
	| bawk '!/^[\#+,$$]/ { \
	split($$0,a,"+ 0 "); \
	gsub(" ","",a[1]); \
	print a[1]; }' \
	| transpose \
	| sed 's/^/$*\t/'  >$@

# Insert Size Metrics
# recomended use of CollectInsertSizeMetrics from picard v 1.88
INSERT = $(addsuffix .insert_size.txt,$(LOCAL_SAMPLES))
%.insert_size.txt: log tmp reference.fasta %.bam
	$(load_modules); \
	java -Xmx4g \
	-Djava.io.tmpdir=$^2 \
	-jar /iga/stratocluster/packages/sw/bio/picard-tools/1.88/CollectInsertSizeMetrics.jar \
	REFERENCE_SEQUENCE=$^3 \
	HISTOGRAM_FILE=$*.insert_size.pdf \
	METRIC_ACCUMULATION_LEVEL=LIBRARY \
	MINIMUM_PCT=$(MINIMUM_PCT) \   * print data for every categories (FR, TANDEM, RF) *
	VALIDATION_STRINGENCY=SILENT \
	HISTOGRAM_WIDTH=$(INSERT_SIZE_HISTOGRAM_WIDTH) \
	ASSUME_SORTED=false \
	TMP_DIR=$^2 \
	INPUT=$^4 \
	OUTPUT=$@ \
	3>&1 1>&2 2>&3 \
	| tee $</picard-CollectInsertSizeMetrics.$@.log



INSERT_PDF = $(addsuffix .insert_size.pdf,$(LOCAL_SAMPLES))
%.insert_size.pdf: %.insert_size.txt
	touch $@

# CollectInsertSizeMetrics produce a pdf with 2 pages for each file
# here the second page is extracted
SECOND_PAGE_INSERT_PDF = $(addsuffix .insert_size.second_page.pdf,$(LOCAL_SAMPLES))
%.insert_size.second_page.pdf: %.insert_size.pdf
	gs -dBATCH -dNOPAUSE -dSAFER  -dAutoRotatePages=/None -q -dPDFSETTINGS=/prepress -sDEVICE=pdfwrite  -dFirstPage=2 -dLastPage=2 -sOutputFile=$@ $<   * extract second page *


# calculate bias in GC content
CGBIAS = $(addsuffix .GC.bias.txt,$(LOCAL_SAMPLES))
%.GC.bias.txt: log tmp reference.fasta %.bam
	$(call load_modules); \
	java -Xmx4g \
	-XX:MaxPermSize=512m \
	-Djava.io.tmpdir=$^2 \
	-jar $$PICARDTOOLS_ROOT/picard.jar \
	CollectGcBiasMetrics \
	TMP_DIR=$^2 \
	REFERENCE_SEQUENCE=$^3 \
	CHART_OUTPUT=$*.GC.bias.pdf \
	ASSUME_SORTED=false \
	VALIDATION_STRINGENCY=SILENT \
	INPUT=$^4 \
	OUTPUT=$@ \
	3>&1 1>&2 2>&3 \
	| tee $</CollectGcBiasMetrics.$(basename $@).log

# GC content bias graph
CGBIAS_PDF = $(addsuffix .GC.bias.pdf,$(LOCAL_SAMPLES))
%.GC.bias.pdf: %.GC.bias.txt
	touch $@


# [PIPELINE]
# name: Alignment refinement metrics picard
# codename: alignment-refinement-metrics-picard
# code: ce92a9bb-c78b-4204-8b00-2d4795eaf61a
# [step.s9]
# label: Alignment Summary Metrics
SUMMARY_METRICS = $(addsuffix .alignment_summary.txt,$(LOCAL_SAMPLES))
%.alignment_summary.txt: log tmp %.bam
	$(load_modules); \
	java -Xmx4g \
	-Djava.io.tmpdir=$^2 \
	-jar $$PICARDTOOLS_ROOT/picard.jar CollectAlignmentSummaryMetrics \
	INPUT=$^3 \
	OUTPUT=$@ \
	MAX_INSERT_SIZE=$(MAX_INSERT_SIZE) \
	ADAPTER_SEQUENCE=null \
	METRIC_ACCUMULATION_LEVEL=LIBRARY \
	VALIDATION_STRINGENCY=LENIENT \
	TMP_DIR=$^2 \
	3>&1 1>&2 2>&3 \
	| tee $</picard-CollectAlignmentSummaryMetrics.$@.log


# compute coverage in wig format
WIG = $(addsuffix .wig,$(LOCAL_SAMPLES))
%.wig: log reference.fasta %.bam
	$(load_modules); \
	nt-compute-profile \
	--fasta $^2 \
	--input-file $^3 \
	--output-file $@ \
	3>&1 1>&2 2>&3 \
	| tee $</nt-compute-profile.$@.log



# # compute coverage in wig format
# WIG = $(addsuffix .wig,$(LOCAL_SAMPLES))
# %.wig: log reference.fasta %.bam
# 	$(load_modules); \
# 	compute_profile \
# 	--sam-format \
# 	--fasta $^2 \
# 	--input-file $^3 \
# 	--output-file $@ \
# 	3>&1 1>&2 2>&3 \
# 	| tee $</compute_profile.$@.log

.META: %.cov_wig.txt
	1	sample
	2	genome size
	3	genome covered
	4	sum of coverage

COV_WIG_TXT = $(addsuffix .cov_wig.txt,$(LOCAL_SAMPLES))
%.cov_wig.txt: %.wig
	coverage_WIG.pl --WIG $< \
	| tail -n 1 \
	| sed 's/^/$*\t/' >$@


COV_WIG_HISTO = $(addsuffix .cov_wig.histo,$(LOCAL_SAMPLES))
%.cov_wig.histo: %.wig
	histogram.pl --wig $< \
	| sed '1s/^/sample\t$*\n/' >$@



WIG_GZ = $(addsuffix .wig.gz,$(LOCAL_SAMPLES))
%.wig.gz: %.wig
	gzip -c $< >$@

# print histogram of coverage
COV_PDF = $(addsuffix .cov_wig.pdf,$(LOCAL_SAMPLES))
%.cov_wig.pdf: %.cov_wig.histo
	bawk '/^0\t[0-9]+/ {flag=1;} /^$$/{flag=0} flag {print}' $< \
	| cov_plot -M $(MAX_COV) -t "$*" -Y $(MAX_MB) -o $@


chrs.size: reference.fasta
	fasta_length <$< >$@


BW = $(addsuffix .bw,$(LOCAL_SAMPLES))
# tranform to compressed bigWig
%.bw: %.wig chrs.size
	$(load_modules); \
	wigToBigWig $< $^2 $@


# compute per reference coverage considering unmapped positions
SEQ_UNMAP_COVERAGE = $(addsuffix .per_reference_coverage_with_unmapped.txt,$(LOCAL_SAMPLES))
%.per_reference_coverage_with_unmapped.txt: log %.bam
	$(load_modules); \
	qaCompute $^2 \
	-m \   * Compute median coverage for each contig/chromosome *
	-q 0 \   * BWA give mapping quality 0 for reads that map with equal quality in multiple places. If you want to condier this, set q to 0 *
	-i \   * silent *
	$@ \
	2>&1 \
	| tee $</qaCompute.$@.log


# compute per reference coverage avoiding unmapped positions
SEQ_COVERAGE = $(addsuffix .per_reference_coverage.txt,$(LOCAL_SAMPLES))

.META: $(SEQ_COVERAGE)
	1	sequence name	sequence_0
	2	lenght	200200
	3	covered bases	200189
	4	mapped bases	831425
	5	mean coverage over covered bases	73.258
	6	median coverage over covered bases	72.000
	7	stdev coverage over covered bases	21.983

%.per_reference_coverage.txt: reference.fasta %.bam
	$(load_modules); \
	samtools depth $^2 \   * It calculates coverage over regions but does not account for regions that were not covered with a read *
	| bawk 'function stdev(sumsq, sum, i) { return sqrt(sumsq/i - (sum/i)**2); } \
	function median(v) { c=asort(v,j); if (c % 2) return j[(c+1)/2]; else return (j[c/2+1]+j[c/2])/2; } \
	BEGIN \
	{ \
		start=""; \
		sum=0; \
		i=0; \
		sumsq=0; \
		delete a; \
	} \
	!/^[\#+,$$]/ { \
	if ( NR == 1 ) \
	{ \
		start=$$1; \
		if ( $$3 > 0 ) \   * avoid non-covered bases *
		{ \
			i++; \
			sum+=$$3; \
			sumsq+=$$3*$$3; \
			a[i]=$$3; \
		} \
	} \
	else \
	{ \
		if ( $$1 != start ) \   * from the second line onwards perform tests between the current line and the precendet *
		{ \
			printf "%s\t%i\t%i\t%.2f\t%.2f\t%.2f\n", start, i, sum, sum/i, median(a), stdev(sumsq, sum, i); \
			i=0; \   * reset array index *
			sum=0; \
			sumsq=0; \
			delete a; \
			start=$$1; \
		} \
		if ($$3 > 0 ) \   * avoid non-covered bases *
		{ \
			i++; \
			sum+=$$3; \
			sumsq+=$$3*$$3; \
			a[i]=$$3; \
		} \
	} \
	} \
	END { \   * print data of last interval *
		printf "%s\t%i\t%i\t%.2f\t%.2f\t%.2f\n", start, i, sum, sum/i, median(a), stdev(sumsq, sum, i); \
	}' \
	| translate -a <(fasta_length <$<) 1 >$@
