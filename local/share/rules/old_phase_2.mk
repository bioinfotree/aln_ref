# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

SAMPLE_NAME ?= 
READS_DIR ?= 
RG ?= 
REFERENCE ?= 

extern ../phase_1/scaffolds.fasta as LOCAL_REF

log:
	mkdir -p $@

# this function install all the links at once
1_FASTQ_GZ = $(addsuffix .1.fastq.gz,$(SAMPLES))
2_FASTQ_GZ = $(addsuffix .2.fastq.gz,$(SAMPLES))
RG_FASTQ_GZ = $(addsuffix .RG,$(SAMPLES))
%.1.fastq.gz  %.2.fastq.gz  %.RG:
	@echo installing links ... \
	$(foreach SAMPLE,$(SAMPLES), \
		$(shell ln -sf $(call get,$(SAMPLE),1) $(SAMPLE).1.fastq.gz) \
		$(shell ln -sf $(call get,$(SAMPLE),2) $(SAMPLE).2.fastq.gz) \
		$(shell echo $(call get,$(SAMPLE),RG) >$(SAMPLE).RG) \
	)

# for mate-pairs
ifeq ($(REV_COMP),TRUE)
%.revcomp.fastq.gz:  %.fastq.gz
	$(load_modules); \
	zcat <$< \
	| fastx_reverse_complement -Q 33 -z >$@
endif


NODUP_BAM = $(addsuffix .sort.nodup.bam,$(SAMPLES))
NODUP_UNIQ_BAM = $(addsuffix .sort.nodup.uniq.clean.RG.fixmate.reord.bam,$(SAMPLES))
TMP = $(addsuffix _tmp,$(SAMPLES))

ifeq ($(REV_COMP),TRUE)
%.sort.nodup.uniq.clean.RG.fixmate.reord.bam: log %.1.revcomp.fastq.gz  %.2.revcomp.fastq.gz  %.RG
else
%.sort.nodup.uniq.clean.RG.fixmate.reord.bam: log %.1.fastq.gz  %.2.fastq.gz  %.RG
endif
	!threads
	$(load_modules); \
	export SAMPLE_NAME=$*; \
	pipeline_align_on_assembly \
	$^2 \
	$^3 \
	$$PWD \   * output dir *
	"$$(cat $^4)" \
	$(LOCAL_REF) \   * bwa indexes *
	$< \
	| tee $</$(basename $@).log

.PHONY: test
test:
	@echo $()


ALL +=  $(NODUP_UNIQ_BAM)

INTERMEDIATE +=

CLEAN += log \
	 $(1_FASTQ_GZ) \
	 $(2_FASTQ_GZ) \
	 $(RG_FASTQ_GZ) \
	 $(NODUP_BAM) \
	 $(TMP)