# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

context prj/aln_genomes

extern ../phase_3/fstats.all as MAPPED_FSTATS
extern ../phase_4/fstats.all as RMDUP_FSTATS
extern ../phase_5/fstats.all as UNIQUE_FSTATS

extern ../phase_3/cov_wig.all as MAPPED_COV
extern ../phase_4/cov_wig.all as RMDUP_COV
extern ../phase_5/cov_wig.all as UNIQUE_COV


mapped.fstats:
	ln -sf $(MAPPED_FSTATS) $@

rmdup.fstats:
	ln -sf $(RMDUP_FSTATS) $@

unique.fstats:
	ln -sf $(UNIQUE_FSTATS) $@


mapped.cov:
	ln -sf $(MAPPED_COV) $@

rmdup.cov:
	ln -sf $(RMDUP_COV) $@

unique.cov:
	ln -sf $(UNIQUE_COV) $@

.META: final.tab
	1	sample
	2	mapped reads (%)
	3	rmdup reads (%)
	4	unique reads (%)
	5	properly paired (%)
	6	mapped cov
	7	rmdup cov
	8	unique cov
	9	mapped genome covered
	10	rmdup genome covered
	11	unique genome covered
	12	mappe covered cov
	13	rmdup covered cov
	14	unique covered cov

final.tab: unique.cov rmdup.cov mapped.cov unique.fstats rmdup.fstats mapped.fstats
	translate -a $^2 1 <$< \
	| translate -a $^3 1 \
	| translate -a $^4 1 \
	| translate -a $^5 1 \
	| translate -a $^6 1 \
	| bawk '!/^[\#+,$$]/ { \
	sample=$$1; \
	mapped_reads=0; rmdup_reads=0; unique_reads=0; properly_paired=0; \
	if ( $$2 != 0 ) { \
		mapped_reads=$$4/$$2; \
		rmdup_reads=$$15/$$2; \
		unique_reads=$$26/$$2; \
		properly_paired=$$30/$$2; } \
	genome_size=$$35; \
	mapped_cov=rmdup_cov=unique_cov=0; \
	if ( genome_size !=0 ) { \
		mapped_cov=$$37/genome_size; \
		rmdup_cov=$$40/genome_size; \
		unique_cov=$$43/genome_size; } \
	mapped_gcov=$$36; \
	rmdup_gcov=$$39; \
	unique_gcov=$$42; \
	mapped_gcovc=rmdup_gcovc=unique_gcovc=0; \
	if ( mapped_gcov != 0 ) mapped_gcovc=$$37/mapped_gcov; \
	if ( rmdup_gcov != 0 ) rmdup_gcovc=$$40/rmdup_gcov; \
	if ( unique_gcov != 0 ) unique_gcovc=$$43/unique_gcov; \
	printf "%s\t%.4f\t%.4f\t%.4f\t%.4f\t%.4f\t%.4f\t%.4f\t%i\t%i\t%i\t%.4f\t%.4f\t%.4f\n", \
		sample, mapped_reads, rmdup_reads, unique_reads, properly_paired, mapped_cov, rmdup_cov, \
		unique_cov, mapped_gcov, rmdup_gcov, unique_gcov, mapped_gcovc, rmdup_gcovc, unique_gcovc; \
	}' >$@



.PHONY: test
test:
	@echo


ALL += final.tab

INTERMEDIATE +=

CLEAN += $(wildcard *.fstats) \
	 $(wildcard *.cov)