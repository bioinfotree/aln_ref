# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

SAMPLE_NAME ?= 
READS_DIR ?= 
RG ?= 
REFERENCE ?= 

context prj/aln_genomes

extern ../phase_1/scaffolds.fasta as LOCAL_REF
extern ../phase_1/scaffolds.fasta.fai as FASTA_INDEX

log:
	mkdir -p $@

scaffolds.fasta:
	ln -sf $(REFERENCE) $@

scaffolds.fasta.fai:
	ln -sf $(FASTA_INDEX) $@

BAM = $(addsuffix .fingerprint.png,$(SAMPLES))
.PHONY:  install_links
install_links:
	@echo installing links ... \
	$(foreach SAMPLE,$(SAMPLES), \
		$(shell ln -sf ../phase_2/$(SAMPLE).sort.nodup.uniq.clean.RG.fixmate.reord.bam $(SAMPLE).bam) \
	)


%.bam: install_links
	sleep 1


LAST = $(addsuffix .per_chr_coverage.txt,$(SAMPLES))
TMP = $(addsuffix _tmp,$(SAMPLES))
%.per_chr_coverage.txt: log %.bam
	!threads
	$(load_modules); \
	export SAMPLE_NAME=$*; \
	align_stats \
	$^2 \
	$$PWD \   * output dir *
	$(LOCAL_REF) \   * bwa indexes *
	$< \
	| tee $</$(basename $@).log


ALL +=  scaffolds.fasta \
	$(LAST)

INTERMEDIATE +=

CLEAN += log \
	 $(TMP) \
	 $(BAM) \
	 $(wildcard $(addsuffix *,$(SAMPLES)))