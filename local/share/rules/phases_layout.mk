# align reads on reference
phase phase_2
# calculates flags and coverage statistics of *.RG.bam.sort
phase phase_3
# calculates flags and coverage statistics of *.RG.bam.sort.nodup
phase phase_4
# calculates all statistics of *.RG.bam.sort.nodup.uniq.clean.fixmate.reord
phase phase_5
# merge all *.RG.bam.sort.nodup.uniq.clean.fixmate.reord and calculates alla statistics
phase phase_6
# summarizes the statistics of the alignments
phase phase_7