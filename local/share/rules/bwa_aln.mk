# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

SAI_1 = $(addsuffix _1.sai,$(SAMPLES))

.PRECIOUS: $(SAI_1)
ifeq ($(REV_COMP),TRUE)
# $(1_FASTQ_GZ_REVCOMP) must be defined as prerequisite in order to
# ensure make produce all revcomp sequences before starting
# alignments. This avoid "*** read jobs pipe: Resource temporarily unavailable.  Stop."
# error
%_1.sai: log %.1.fastq.gz.revcomp $(1_FASTQ_GZ_REVCOMP)
else
%_1.sai: log %.1.fastq.gz
endif
	!threads
	$(load_modules); \
	bwa aln \
	-t $$THREADNUM \
	$(LOCAL_REF) \
	$^2 \
	2>$@ 3>&1 1>&2 2>&3 \   * redirect stdout to file, stderr to stdout and file *
	| tee $</bwa-aln.$@.log   * [tool].[taget].log *


SAI_2 = $(addsuffix _2.sai,$(SAMPLES))

.PRECIOUS: $(SAI_2)
ifeq ($(REV_COMP),TRUE)
%_2.sai: log %.2.fastq.gz.revcomp $(2_FASTQ_GZ_REVCOMP)
else
%_2.sai: log %.2.fastq.gz
endif
	!threads
	$(load_modules); \
	bwa aln \
	-t $$THREADNUM \
	$(LOCAL_REF) \
	$^2 \
	2>$@ 3>&1 1>&2 2>&3 \   * redirect stdout to file, stderr to stdout and file *
	| tee $</bwa-aln.$@.log   * [tool].[taget].log *



# [PIPELINE]
# name: BWAaln_PE-SE_metrics_RG
# codename: bwaaln-pe-se-metrics-rg
# code: 71b3f899-3498-4b98-9595-d2a5c9f43cb5
# [step.s2] sampe
BAM = $(addsuffix .RG.bam,$(SAMPLES))
ifeq ($(REV_COMP),TRUE)
%.RG.bam: log %_1.sai %_2.sai %.1.fastq.gz.revcomp %.2.fastq.gz.revcomp %.RG
else
%.RG.bam: log %_1.sai %_2.sai %.1.fastq.gz %.2.fastq.gz %.RG
endif
	$(load_modules); \
	bwa sampe \
	-r "$$(cat $^6)" \
	$(LOCAL_REF) \
	$^2 $^3 \
	$^4 $^5 \
	| samtools view -bS - \
	2>$@ 3>&1 1>&2 2>&3 \
	| tee $</bwa-sampe.$@.log



UNIQ = $(addsuffix .RG.bam.sort.nodup.uniq,$(SAMPLES))
%.RG.bam.sort.nodup.uniq: log %.RG.bam.sort.nodup
	!threads
	$(load_modules); \
	TMP=$$(mktemp XXXXXXXXXX.sam); \
	samtools view -H $^2 >$$TMP \
	&& samtools view $^2 \
	| grep "XT:A:U" >>$$TMP \
	&& samtools view -bSq 10 $$TMP \
	| samtools sort -o -@ $$THREADNUM - - \
	2>$@ 3>&1 1>&2 2>&3 \
	| tee $</samtools-view.$@.log \
	&& rm $$TMP


ALL += 	$(SAI_1) \
	$(SAI_2)

CLEAN += $(SAI_1) \
	 $(SAI_2)