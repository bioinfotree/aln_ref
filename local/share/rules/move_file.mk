# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

# [PIPELINE]
# name: BWAaln_PE-SE_metrics_RG
# codename: bwaaln-pe-se-metrics-rg
# code: 71b3f899-3498-4b98-9595-d2a5c9f43cb5
# [step.s6] clean_sam
FURTHER_CLEAN_SAM = $(addsuffix .RG.bam.sort.nodup.clean,$(SAMPLES))
%.RG.bam.sort.nodup.clean: log tmp %.RG.bam.sort.nodup
	!threads
	$(load_modules); \
	java -Xmx2g \
	-XX:ParallelGCThreads=$$THREADNUM \
	-Djava.io.tmpdir=$^2 \
	-jar $$PICARDTOOLS_ROOT/picard.jar CleanSam \
	INPUT=$^3 \
	OUTPUT=$@ \
	VALIDATION_STRINGENCY=STRICT \
	TMP_DIR=$^2 \
	3>&1 1>&2 2>&3 \
	| tee $</picard-CleanSam.$@.log


# [PIPELINE]
# name: Alignment refinement metrics picard
# codename: alignment-refinement-metrics-picard
# code: ce92a9bb-c78b-4204-8b00-2d4795eaf61a
# [step.s5]
# label: Fix Mate Information
# fix mate information
FURTHER_FIXMATE = $(addsuffix .RG.bam.sort.nodup.clean.fixmate,$(SAMPLES))
%.RG.bam.sort.nodup.clean.fixmate: log tmp %.RG.bam.sort.nodup.clean
	!threads
	$(load_modules); \
	java -Xmx4g \
	-XX:ParallelGCThreads=$$THREADNUM \
	-Djava.io.tmpdir=$^2 \
	-jar $$PICARDTOOLS_ROOT/picard.jar FixMateInformation \
	INPUT=$^3 \
	OUTPUT=$@ \
	TMP_DIR=$^2 \
	SORT_ORDER=coordinate \
	VALIDATION_STRINGENCY=LENIENT \
	3>&1 1>&2 2>&3 \
	| tee $</picard-FixMateInformation.$@.log


# [PIPELINE]
# name: Alignment refinement metrics picard
# codename: alignment-refinement-metrics-picard
# code: ce92a9bb-c78b-4204-8b00-2d4795eaf61a
# [step.s7]
# label: Reorder SAM
FURTHER_REORD = $(addsuffix .RG.bam.sort.nodup.clean.fixmate.reord,$(SAMPLES))
%.RG.bam.sort.nodup.clean.fixmate.reord: log tmp %.RG.bam.sort.nodup.clean.fixmate reference.fasta.dict
	!threads
	$(load_modules); \
	java -Xmx4g \
	-XX:ParallelGCThreads=$$THREADNUM \
	-Djava.io.tmpdir=$^2 \
	-jar $$PICARDTOOLS_ROOT/picard.jar ReorderSam \
	INPUT=$^3 \
	OUTPUT=$@ \
	TMP_DIR=$^2 \
	REFERENCE=$(basename $^4) \
	VALIDATION_STRINGENCY=LENIENT \
	COMPRESSION_LEVEL=9 \   * maximum compression level *
	3>&1 1>&2 2>&3 \
	| tee $</picard-ReorderSam.$@.log



.PHONY: move_file
move_file:
	$(load_modules); \
	CURRENT="$$PWD"; \
	TOKEN=( $${PWD//[\/]/ } ); \
	SAMPLE=$${TOKEN[$${#TOKEN[@]}-2]^^}; \
	TYPE=$${TOKEN[$${#TOKEN[@]}-3]}; \
	FINAL_DEST="$(FINAL_DEST_BASENAME_ALN)/$$SAMPLE/$$TYPE"; \
	function to_stdout () { echo -e "[$@]:\t$$1"; }; \
	if [ ! -d "$$FINAL_DEST" ]; then \
		to_stdout "create dir $$FINAL_DEST"; \
		mkdir -p $$FINAL_DEST; \
	fi; \
	function move () { \
		if [ -s "$$1" ]; then \
			if [ ! -s "$$FINAL_DEST/$$2" ]; then \
				to_stdout "move file $$1 to $$FINAL_DEST/$$2"; \
				mv "$$1" "$$FINAL_DEST/$$2"; \
				to_stdout "link $$FINAL_DEST/$$2 to $$1"; \
				ln -sf "$$FINAL_DEST/$$2" "$$1"; \
				to_stdout "generate index for $$FINAL_DEST/$$2"; \
				cd "$$FINAL_DEST"; \
				samtools index "$$2"; \
				cd "$$CURRENT"; \
			else \
				to_stdout "file $$FINAL_DEST/$$2 already exist!"; \
			fi; \
		fi; \
	}; \
	for F in $(FURTHER_REORD); do \
		NEW_F="$${F%.RG.bam.sort.nodup.clean.fixmate.reord}.bam"; \
		move "$$F" "$$NEW_F"; \
	done; \
	for F in $(REORD); do \
		NEW_F="$${F%.RG.bam.sort.nodup.uniq.clean.fixmate.reord}_U.bam"; \
		move "$$F" "$$NEW_F"; \
	done;



# ALL += $(FURTHER_CLEAN_SAM) \
	# $(FURTHER_FIXMATE) \
	# $(FURTHER_REORD)

INTERMEDIATE += 