# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

SAMPLE_NAME ?= 
READS_DIR ?= 
RG ?= 
REFERENCE ?= 


log:
	mkdir -p $@

tmp:
	mkdir -p $@

reference.fasta:
	ln -sf $(REFERENCE) $@

# this function install all the links at once
1_FASTQ_GZ = $(addsuffix .1.fastq.gz,$(SAMPLES))
2_FASTQ_GZ = $(addsuffix .2.fastq.gz,$(SAMPLES))
RG_FASTQ_GZ = $(addsuffix .RG,$(SAMPLES))
%.1.fastq.gz  %.2.fastq.gz  %.RG:
	@echo installing links ... \
	$(foreach SAMPLE,$(SAMPLES), \
		$(shell ln -sf $(call get,$(SAMPLE),1) $(SAMPLE).1.fastq.gz) \
		$(shell ln -sf $(call get,$(SAMPLE),2) $(SAMPLE).2.fastq.gz) \
		$(shell echo -E '$(call get,$(SAMPLE),RG)' >$(SAMPLE).RG) \
	) \
	&& sleep 3

# for mate-pairs
ifeq ($(REV_COMP),TRUE)
1_FASTQ_GZ_REVCOMP = $(addsuffix .1.fastq.gz.revcomp,$(SAMPLES))
2_FASTQ_GZ_REVCOMP = $(addsuffix .2.fastq.gz.revcomp,$(SAMPLES))
%.1.fastq.gz.revcomp: %.1.fastq.gz
	$(load_modules); \
	zcat <$< \
	| fastx_reverse_complement -Q 33 -z >$@

%.2.fastq.gz.revcomp: %.2.fastq.gz
	$(load_modules); \
	zcat <$< \
	| fastx_reverse_complement -Q 33 -z >$@
endif




ifeq ($(BWA),MEM) ############# CHOICE TO USE MEM OR ALN START HERE; CHOICE MEM ###################################

BAM = $(addsuffix .RG.bam,$(SAMPLES))

.PRECIOUS: $(BAM)

ifeq ($(REV_COMP),TRUE)
%.RG.bam: log %.RG %.1.fastq.gz.revcomp %.2.fastq.gz.revcomp $(1_FASTQ_GZ_REVCOMP)
else
%.RG.bam: log %.RG %.1.fastq.gz %.2.fastq.gz
endif
	!threads
	$(load_modules); \
	bwa mem \
	-t $$THREADNUM \
	-M \
	-R "$$(cat $^2)" \
	$(REFERENCE) \
	$^3 $^4 \
	2>$</bwa-mem.$@.log \
	| samtools view -bS - \
	>$@


UNIQ = $(addsuffix .RG.bam.sort.nodup.uniq,$(SAMPLES))
# remove secondary alignments (bwa.-mem -M option marks SUPPLEMENTARY alignments ad SECONDARY)
%.RG.bam.sort.nodup.uniq: log %.RG.bam.sort.nodup
	!threads
	$(load_modules); \
	samtools view -bhq 10 -F "0x0100" $^2 \
	| samtools sort -o -@ $$THREADNUM - - \
	2>$@ 3>&1 1>&2 2>&3 \
	| tee $</samtools-view.$@.log



else  ############################################## ELSE CHOICE ALN ####################################

SAI_1 = $(addsuffix _1.sai,$(SAMPLES))

.PRECIOUS: $(SAI_1)
ifeq ($(REV_COMP),TRUE)
# $(1_FASTQ_GZ_REVCOMP) must be defined as prerequisite in order to
# ensure make produce all revcomp sequences before starting
# alignments. This avoid "*** read jobs pipe: Resource temporarily unavailable.  Stop."
# error
%_1.sai: log %.1.fastq.gz.revcomp $(1_FASTQ_GZ_REVCOMP)
else
%_1.sai: log %.1.fastq.gz
endif
	!threads
	$(load_modules); \
	bwa aln \
	-t $$THREADNUM \
	$(REFERENCE) \
	$^2 \
	2>$@ 3>&1 1>&2 2>&3 \   * redirect stdout to file, stderr to stdout and file *
	| tee $</bwa-aln.$@.log   * [tool].[taget].log *


SAI_2 = $(addsuffix _2.sai,$(SAMPLES))

.PRECIOUS: $(SAI_2)
ifeq ($(REV_COMP),TRUE)
%_2.sai: log %.2.fastq.gz.revcomp $(2_FASTQ_GZ_REVCOMP)
else
%_2.sai: log %.2.fastq.gz
endif
	!threads
	$(load_modules); \
	bwa aln \
	-t $$THREADNUM \
	$(REFERENCE) \
	$^2 \
	2>$@ 3>&1 1>&2 2>&3 \   * redirect stdout to file, stderr to stdout and file *
	| tee $</bwa-aln.$@.log   * [tool].[taget].log *



# [PIPELINE]
# name: BWAaln_PE-SE_metrics_RG
# codename: bwaaln-pe-se-metrics-rg
# code: 71b3f899-3498-4b98-9595-d2a5c9f43cb5
# [step.s2] sampe
BAM = $(addsuffix .RG.bam,$(SAMPLES))
ifeq ($(REV_COMP),TRUE)
%.RG.bam: log %_1.sai %_2.sai %.1.fastq.gz.revcomp %.2.fastq.gz.revcomp %.RG
else
%.RG.bam: log %_1.sai %_2.sai %.1.fastq.gz %.2.fastq.gz %.RG
endif
	$(load_modules); \
	bwa sampe \
	-r "$$(cat $^6)" \
	$(REFERENCE) \
	$^2 $^3 \
	$^4 $^5 \
	| samtools view -bS - \
	2>$@ 3>&1 1>&2 2>&3 \
	| tee $</bwa-sampe.$@.log



UNIQ = $(addsuffix .RG.bam.sort.nodup.uniq,$(SAMPLES))
%.RG.bam.sort.nodup.uniq: log %.RG.bam.sort.nodup
	!threads
	$(load_modules); \
	TMP=$$(mktemp XXXXXXXXXX.sam); \
	samtools view -H $^2 >$$TMP \
	&& samtools view $^2 \
	| grep "XT:A:U" >>$$TMP \
	&& samtools view -bSq 10 $$TMP \
	| samtools sort -o -@ $$THREADNUM - - \
	2>$@ 3>&1 1>&2 2>&3 \
	| tee $</samtools-view.$@.log \
	&& rm $$TMP


ALL += 	$(SAI_1) \
	$(SAI_2)

CLEAN += $(SAI_1) \
	 $(SAI_2)


endif   ############# CHOICE TO USE MEM OR ALN END HERE ###################################


# [PIPELINE]
# name: BWAaln_PE-SE_metrics_RG
# codename: bwaaln-pe-se-metrics-rg
# code: 71b3f899-3498-4b98-9595-d2a5c9f43cb5
# [step.s5] sort bam
SORT = $(addsuffix .RG.bam.sort,$(SAMPLES))
%.RG.bam.sort: log %.RG.bam
	!threads
	$(load_modules); \
	samtools sort \
	-l 9 \   * maximum compression level *
	-o -@ $$THREADNUM \
	$^2 - \
	2>$@ 3>&1 1>&2 2>&3 \
	| tee $</samtools-sort.$@.log


RMDUP = $(addsuffix .RG.bam.sort.nodup,$(SAMPLES))
# %.RG.bam.sort.nodup: log %.RG.bam.sort
# 	$(load_modules); \
# 	samtools rmdup -S $^2 \
# 	$@ \
# 	2>&1 \
# 	| tee $</samtools-rmdup.$@.log


# [PIPELINE]
# name: Alignment refinement metrics picard
# codename: alignment-refinement-metrics-picard
# code: ce92a9bb-c78b-4204-8b00-2d4795eaf61a
# [step.s6]
# label: Mark Duplicates
%.RG.bam.sort.nodup: log tmp %.RG.bam.sort
	!threads
	$(load_modules); \
	java -Xmx4g \
	-XX:ParallelGCThreads=$$THREADNUM \
	-Djava.io.tmpdir=$^2 \
	-jar $$PICARDTOOLS_ROOT/picard.jar MarkDuplicates \
	TMP_DIR=$^2 \
	INPUT=$^3 \
	OUTPUT=$@ \
	MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=1000 \   * to avoid java Java.Io.Filenotfoundexception ( Too Many Open Files ) *
	REMOVE_DUPLICATES=True \
	METRICS_FILE=$@.metrics \
	VALIDATION_STRINGENCY=LENIENT \
	COMPRESSION_LEVEL=9 \   * maximum compression level *
	3>&1 1>&2 2>&3 \
	| tee $</picard-MarkDuplicates.$@.log


RMDUP_METRICS = $(addsuffix .RG.bam.sort.nodup.metrics,$(SAMPLES))
%.RG.bam.sort.nodup.metrics: %.RG.bam.sort.nodup
	touch $@


# UNIQ = $(addsuffix .RG.bam.sort.nodup.uniq,$(SAMPLES))
# %.RG.bam.sort.nodup.uniq: log %.RG.bam.sort.nodup
# 	!threads
# 	$(load_modules); \
# 	TMP=$$(mktemp XXXXXXXXXX.sam); \
# 	samtools view -H $^2 >$$TMP \
# 	&& samtools view $^2 \
# 	| grep "XT:A:U" >>$$TMP \
# 	&& samtools view -bSq 10 $$TMP \
# 	| samtools sort -o -@ $$THREADNUM - - \
# 	2>$@ 3>&1 1>&2 2>&3 \
# 	| tee $</samtools-view.$@.log \
# 	&& rm $$TMP



# [PIPELINE]
# name: BWAaln_PE-SE_metrics_RG
# codename: bwaaln-pe-se-metrics-rg
# code: 71b3f899-3498-4b98-9595-d2a5c9f43cb5
# [step.s6] clean_sam
CLEAN_SAM = $(addsuffix .RG.bam.sort.nodup.uniq.clean,$(SAMPLES))
%.RG.bam.sort.nodup.uniq.clean: log tmp %.RG.bam.sort.nodup.uniq
	!threads
	$(load_modules); \
	java -Xmx2g \
	-XX:ParallelGCThreads=$$THREADNUM \
	-Djava.io.tmpdir=$^2 \
	-jar $$PICARDTOOLS_ROOT/picard.jar CleanSam \
	INPUT=$^3 \
	OUTPUT=$@ \
	VALIDATION_STRINGENCY=STRICT \
	TMP_DIR=$^2 \
	3>&1 1>&2 2>&3 \
	| tee $</picard-CleanSam.$@.log



# # [PIPELINE]
# # name: BWAaln_PE-SE_metrics_RG
# # codename: bwaaln-pe-se-metrics-rg
# # code: 71b3f899-3498-4b98-9595-d2a5c9f43cb5
# # [step.s7] Add_RG_tags
# ADD_RG = $(addsuffix .bam.RG.sort.nodup.uniq.clean,$(SAMPLES))
# %.bam.RG.sort.nodup.uniq.clean: log tmp %.bam.sort.nodup.uniq.clean %.RG
# 	$(load_modules); \
# 	java -Xmx4g \
# 	-Djava.io.tmpdir=$^2 \
# 	-jar $$PICARDTOOLS_ROOT/picard.jar AddOrReplaceReadGroups \
# 	INPUT=$^3 \
# 	OUTPUT=$@ \
# 	$$(cat $^4) \
# 	VALIDATION_STRINGENCY=LENIENT \
# 	TMP_DIR=$^2 \
# 	3>&1 1>&2 2>&3 \
# 	| tee $</picard-AddOrReplaceReadGroups.$@.log



# [PIPELINE]
# name: Alignment refinement metrics picard
# codename: alignment-refinement-metrics-picard
# code: ce92a9bb-c78b-4204-8b00-2d4795eaf61a
# [step.s5]
# label: Fix Mate Information
# fix mate information
FIXMATE = $(addsuffix .RG.bam.sort.nodup.uniq.clean.fixmate,$(SAMPLES))
%.RG.bam.sort.nodup.uniq.clean.fixmate: log tmp %.RG.bam.sort.nodup.uniq.clean
	!threads
	$(load_modules); \
	java -Xmx4g \
	-XX:ParallelGCThreads=$$THREADNUM \
	-Djava.io.tmpdir=$^2 \
	-jar $$PICARDTOOLS_ROOT/picard.jar FixMateInformation \
	INPUT=$^3 \
	OUTPUT=$@ \
	TMP_DIR=$^2 \
	SORT_ORDER=coordinate \
	VALIDATION_STRINGENCY=LENIENT \
	3>&1 1>&2 2>&3 \
	| tee $</picard-FixMateInformation.$@.log





reference.fasta.dict: log tmp reference.fasta
	!threads
	$(load_modules); \
	if [ -s "$@" ]; then \
		rm $@; \   * to avoid errors if already exists *
	fi; \
	java -Xmx4g \
	-XX:ParallelGCThreads=$$THREADNUM \
	-Djava.io.tmpdir=$^2 \
	-jar $$PICARDTOOLS_ROOT/picard.jar CreateSequenceDictionary \
	REFERENCE=$(REFERENCE) \
	OUTPUT=$@ \
	TMP_DIR=$^2 \
	3>&1 1>&2 2>&3 \
	| tee $</picard-CreateSequenceDictionary.$@.log




# [PIPELINE]
# name: Alignment refinement metrics picard
# codename: alignment-refinement-metrics-picard
# code: ce92a9bb-c78b-4204-8b00-2d4795eaf61a
# [step.s7]
# label: Reorder SAM
REORD = $(addsuffix .RG.bam.sort.nodup.uniq.clean.fixmate.reord,$(SAMPLES))
%.RG.bam.sort.nodup.uniq.clean.fixmate.reord: log tmp %.RG.bam.sort.nodup.uniq.clean.fixmate reference.fasta.dict
	!threads
	$(load_modules); \
	java -Xmx4g \
	-XX:ParallelGCThreads=$$THREADNUM \
	-Djava.io.tmpdir=$^2 \
	-jar $$PICARDTOOLS_ROOT/picard.jar ReorderSam \
	INPUT=$^3 \
	OUTPUT=$@ \
	TMP_DIR=$^2 \
	REFERENCE=$(basename $^4) \
	VALIDATION_STRINGENCY=LENIENT \
	COMPRESSION_LEVEL=9 \   * maximum compression level *
	3>&1 1>&2 2>&3 \
	| tee $</picard-ReorderSam.$@.log


import move_file


.PHONY: test
test:
	@echo $(FURTHER_CLEAN_SAM) $(FURTHER_FIXMATE) $(FURTHER_REORD) reference.fasta.dict

ALL +=  log \
	reference.fasta \
	$(1_FASTQ_GZ) \
	$(2_FASTQ_GZ) \
	$(RG_FASTQ_GZ) \
	$(SORT) \
	$(RMDUP) \
	$(RMDUP_METRICS) \
	$(REORD) \
	$(FURTHER_CLEAN_SAM) \
	$(FURTHER_FIXMATE) \
	$(FURTHER_REORD)

INTERMEDIATE += reference.fasta.dict \
	$(1_FASTQ_GZ_REVCOMP) \
	$(2_FASTQ_GZ_REVCOMP) \
	$(BAM)

CLEAN += tmp \
	 $(1_FASTQ_GZ) \
	 $(2_FASTQ_GZ) \
	 $(RG_FASTQ_GZ) \
	 $(SORT)
