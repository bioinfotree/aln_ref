# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>


BAM = $(addsuffix .RG.bam,$(SAMPLES))

.PRECIOUS: $(BAM)

ifeq ($(REV_COMP),TRUE)
%.RG.bam: log %.RG %.1.fastq.gz.revcomp %.2.fastq.gz.revcomp $(1_FASTQ_GZ_REVCOMP)
else
%.RG.bam: log %.RG %.1.fastq.gz %.2.fastq.gz
endif
	!threads
	$(load_modules); \
	bwa mem \
	-t $$THREADNUM \
	-M \
	-R "$$(cat $^2)" \
	$(LOCAL_REF) \
	$^3 $^4 \
	2>$</bwa-mem.$@.log \
	| samtools view -bS - \
	>$@


UNIQ = $(addsuffix .RG.bam.sort.nodup.uniq,$(SAMPLES))
%.RG.bam.sort.nodup.uniq: log %.RG.bam.sort.nodup
	!threads
	$(load_modules); \
	samtools view -bhq 20 -F "0x0100" $^2 \
	| samtools sort -o -@ $$THREADNUM - - \
	2>$@ 3>&1 1>&2 2>&3 \
	| tee $</samtools-view.$@.log
