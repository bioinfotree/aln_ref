# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

REFERENCE ?= 

log:
	mkdir -p $@

reference.fasta:
	zcat <$(REFERENCE) \
	| fasta2tab \
	| tr " " \\t \
	| awk 'BEGIN { OFS = "\t" } !/^[\#+,$$]/ { print $$1,$$NF; }' \
	| bsort \
	| tab2fasta 2 >$@

reference.fasta.bwt: log reference.fasta
	$(load_modules); \
	bwa index $^2 \
	2>&1 \
	| tee $</$(basename $@).log


ALL +=  reference.fasta \
	reference.fasta.bwt

INTERMEDIATE +=

CLEAN += log \
	 $(wildcard reference.fasta.*)
