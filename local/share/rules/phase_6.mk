# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

SAMPLE_NAME ?= 
READS_DIR ?= 
RG ?= 
REFERENCE ?= 
INSERT_SIZE_HISTOGRAM_WIDTH ?= null
MAX_INSERT_SIZE ?= 100000
MINIMUM_PCT ?= 0.05
MAX_MB ?= 25
MAX_COV ?= 50


context prj/aln_genomes

log:
	mkdir -p $@

tmp:
	mkdir -p $@

reference.fasta:
	ln -sf $(REFERENCE) $@


merged.nodup.clean.fixmate.reord.bam: log tmp
	$(load_modules); \
	java -Xmx4g \
	-Djava.io.tmpdir=$^2 \
	-jar $$PICARDTOOLS_ROOT/picard.jar MergeSamFiles \
	TMP_DIR=$^2 \
	$(addprefix INPUT=../phase_2/,$(addsuffix .RG.bam.sort.nodup.clean.fixmate.reord,$(SAMPLES))) \
	OUTPUT=$@ \
	USE_THREADING=True \
	MERGE_SEQUENCE_DICTIONARIES=True \
	VALIDATION_STRINGENCY=LENIENT \
	COMPRESSION_LEVEL=9 \   * maximum compression level *
	3>&1 1>&2 2>&3 \
	| tee $</picard-MergeSamFiles.$@.log

LOCAL_SAMPLES = merged.nodup.clean.fixmate.reord

import calculate_stat

.META: fstats.all
	1	sample
	2	in total (QC-passed reads + QC-failed reads)
	3	duplicates
	4	mapped
	5	paired in sequencing
	6	read1
	7	read2
	8	properly paired
	9	with itself and mate mapped
	10	singletons
	11	with mate mapped to a different chr
	12	with mate mapped to a different chr (mapQ>=5)

fstats.all: $(FSTATS)
	cat $^ >$@

.META: cov_wig.all
	1	sample
	2	genome size
	3	genome covered
	4	sum of coverage

cov_wig.all: $(COV_WIG_TXT)
	cat $^ >$@


.META: insert_size.all
	1	SAMPLE
	2	MEDIAN_INSERT_SIZE
	3	MEDIAN_ABSOLUTE_DEVIATION
	4	PAIR_ORIENTATION
	5	FRACTION_READ_PAIRS_RESPECT_TO_ALIGNED
	6	READ_PAIRS
	7	MIN_INSERT_SIZE
	8	MAX_INSERT_SIZE
	9	MEAN_INSERT_SIZE
	10	STANDARD_DEVIATION
	11	SAMPLE
	12	LIBRARY
	13	READ_GROUP



insert_size.all: $(INSERT) fstats.all
	for FILE in $(INSERT); do \
		SAMPLE=$$(grep -o -E 'INPUT=[A-Za-z0-9_-.]+' $$FILE \
		| tr '=' \\t \
		| cut -f2); \
		bawk '/^MEDIAN_INSERT_SIZE/ {flag=1;next} /^$$/{flag=0} flag { print $$0; }' <$$FILE \
		| bawk -v sample=$${SAMPLE%.*} '! /^[#+,$$]/ { if ( $$19 != "" && $$20 != "" ) print sample, $$0; }'; \   * print only lines with SAMPLE and LIBRARY reported *
	done \
	| translate -a $(call last,$^) 1 \
	|  bawk '! /^[#+,$$]/ { \
	print $$1, $$13, $$14, $$20, $$19/($$2/2), $$19, \   * % of pairs mapped in given orietation: n° of pairs // ( second argument of fstats.all // 2 ) *
	$$15, $$16, $$17, $$18, $$31, $$32, $$33; \
	}' \
	| bsort -k 1,1 -k 6,6rg \
	| uniq \
	| tr ',' '.'>$@   * all decimal separator should me , *

insert_size.all.png: $(SECOND_PAGE_INSERT_PDF)
	montage -density 150x150 -quality 100 -mode concatenate -tile 1x $^ $@


.PHONY: test
test:
	@echo $(PRJ_ROOT)


ALL +=  log \
	reference.fasta \
	merged.nodup.clean.fixmate.reord.bam \
	$(BAI) \
	$(FSTATS) \
	$(INSERT) \
	$(CHR_COVERAGE) \
	$(INSERT_PDF) \
	$(SUMMARY_METRICS) \
	$(COV_WIG_TXT) \
	$(COV_WIG_HISTO) \
	$(WIG_GZ) \
	$(COV_PDF) \
	$(SEQ_UNMAP_COVERAGE) \
	$(SEQ_COVERAGE) \
	fstats.all \
	cov_wig.all \
	insert_size.all \
	insert_size.all.png

INTERMEDIATE += chrs.size

CLEAN += tmp \
	$(BAI) \
	merged.nodup.bam
