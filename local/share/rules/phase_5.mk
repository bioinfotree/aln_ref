# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

SAMPLE_NAME ?= 
READS_DIR ?= 
RG ?= 
REFERENCE ?= 
INSERT_SIZE_HISTOGRAM_WIDTH ?= null
MAX_INSERT_SIZE ?= 100000
MINIMUM_PCT ?= 0.05
MAX_MB ?= 25
MAX_COV ?= 50


context prj/aln_genomes


log:
	mkdir -p $@

tmp:
	mkdir -p $@

reference.fasta:
	ln -sf $(REFERENCE) $@


BAM = $(addsuffix .bam,$(SAMPLES))
.PHONY:  install_links
%.bam:
	@echo installing links ... \
	$(foreach SAMPLE,$(SAMPLES), \
		$(shell ln -sf ../phase_2/$(SAMPLE).RG.bam.sort.nodup.uniq.clean.fixmate.reord $(SAMPLE).bam) \
	)

LOCAL_SAMPLES = $(SAMPLES)

import calculate_stat


.META: fstats.all
	1	sample
	2	in total (QC-passed reads + QC-failed reads)
	3	duplicates
	4	mapped
	5	paired in sequencing
	6	read1
	7	read2
	8	properly paired
	9	with itself and mate mapped
	10	singletons
	11	with mate mapped to a different chr
	12	with mate mapped to a different chr (mapQ>=5)

fstats.all: $(FSTATS)
	cat $^ >$@

.META: cov_wig.all
	1	sample
	2	genome size
	3	genome covered
	4	sum of coverage

cov_wig.all: $(COV_WIG_TXT)
	cat $^ >$@


.META: insert_size.all
	1	SAMPLE
	2	MEDIAN_INSERT_SIZE
	3	MEDIAN_ABSOLUTE_DEVIATION
	4	PAIR_ORIENTATION
	5	FRACTION_READ_PAIRS_RESPECT_TO_ALIGNED
	6	READ_PAIRS
	7	MIN_INSERT_SIZE
	8	MAX_INSERT_SIZE
	9	MEAN_INSERT_SIZE
	10	STANDARD_DEVIATION
	11	SAMPLE
	12	LIBRARY
	13	READ_GROUP



insert_size.all: $(INSERT) fstats.all
	for FILE in $(INSERT); do \
		SAMPLE=$$(grep -o -E 'INPUT=[A-Za-z0-9_-.]+' $$FILE \
		| tr '=' \\t \
		| cut -f2); \
		bawk '/^MEDIAN_INSERT_SIZE/ {flag=1;next} /^$$/{flag=0} flag { print $$0; }' <$$FILE \
		| bawk -v sample=$${SAMPLE%.*} '! /^[#+,$$]/ { if ( $$19 != "" && $$20 != "" ) print sample, $$0; }'; \   * print only lines with SAMPLE and LIBRARY reported *
	done \
	| translate -a $(call last,$^) 1 \
	|  bawk '! /^[#+,$$]/ { \
	print $$1, $$13, $$14, $$20, $$19/($$2/2), $$19, \   * % of pairs mapped in given orietation: n° of pairs // ( second argument of fstats.all // 2 ) *
	$$15, $$16, $$17, $$18, $$31, $$32, $$33; \
	}' \
	| bsort -k 1,1 -k 6,6rg \
	| uniq \
	| tr ',' '.'>$@   * all decimal separator should me , *

insert_size.all.png: $(SECOND_PAGE_INSERT_PDF)
	montage -density 150x150 -quality 100 -mode concatenate -tile 1x $^ $@
	#gs -dBATCH -dNOPAUSE -dSAFER -dAutoRotatePages=/None -q -dPDFSETTINGS=/prepress -sDEVICE=pdfwrite -sOutputFile=$@ $^   * merge pdf vertically without rotation *


.PHONY: move_file
move_file:
	$(load_modules); \
	CURRENT="$$PWD"; \
	TOKEN=( $${PWD//[\/]/ } ); \
	SAMPLE=$${TOKEN[$${#TOKEN[@]}-2]^^}; \
	TYPE=$${TOKEN[$${#TOKEN[@]}-3]}; \
	FINAL_DEST="$(FINAL_DEST_BASENAME_COV)/$$SAMPLE/$$TYPE"; \
	function to_stdout () { echo -e "[$@]:\t$$1"; }; \
	if [ ! -d "$$FINAL_DEST" ]; then \
		to_stdout "create dir $$FINAL_DEST"; \
		mkdir -p $$FINAL_DEST; \
	fi; \
	function move () { \
		if [ -s "$$1" ]; then \
			if [ ! -s "$$FINAL_DEST/$$2" ]; then \
				to_stdout "move file $$1 to $$FINAL_DEST/$$2"; \
				mv "$$1" "$$FINAL_DEST/$$2"; \
				to_stdout "link $$FINAL_DEST/$$2 to $$1"; \
				ln -sf "$$FINAL_DEST/$$2" "$$1"; \
			else \
				to_stdout "file $$FINAL_DEST/$$2 already exist!"; \
			fi; \
		fi; \
	}; \
	for F in $(COV_WIG_TXT); do \
		NEW_F="$${F%.cov_wig.txt}_U.cov"; \
		move "$$F" "$$NEW_F"; \
	done; \
	for F in $(WIG_GZ); do \
		NEW_F="$${F%.wig.gz}_U.wig.gz"; \
		move "$$F" "$$NEW_F"; \
	done; \
	FINAL_DEST="$(FINAL_DEST_BASENAME_ALN)/$$SAMPLE/$$TYPE"; \
	if [ ! -d "$$FINAL_DEST" ]; then \
		to_stdout "create dir $$FINAL_DEST"; \
		mkdir -p $$FINAL_DEST; \
	fi; \
	for F in $(INSERT_PDF); do \
		NEW_F="$${F%.insert_size.pdf}_U.insert.pdf"; \
		move "$$F" "$$NEW_F"; \
	done; \
	for F in $(INSERT); do \
		NEW_F="$${F%.insert_size.txt}_U.insert.txt"; \
		move "$$F" "$$NEW_F"; \
	done;



ALL +=  log \
	reference.fasta \
	$(BAM) \
	$(BAI) \
	$(FSTATS) \
	$(INSERT) \
	$(CHR_COVERAGE) \
	$(INSERT_PDF) \
	$(CGBIAS) \
	$(CGBIAS_PDF) \
	$(SUMMARY_METRICS) \
	$(COV_WIG_TXT) \
	$(COV_WIG_HISTO) \
	$(WIG_GZ) \
	$(COV_PDF) \
	$(SEQ_UNMAP_COVERAGE) \
	$(SEQ_COVERAGE) \
	fstats.all \
	cov_wig.all \
	insert_size.all \
	insert_size.all.png


INTERMEDIATE += chrs.size

CLEAN += tmp \
	$(BAI) \
	$(SECOND_PAGE_INSERT_PDF)